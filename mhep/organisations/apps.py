from django.apps import AppConfig


class OrganisationsConfig(AppConfig):
    name = "mhep.organisations"
    verbose_name = "Organisations"
