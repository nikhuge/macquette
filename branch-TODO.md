Now:
- CI builds Docker image and pushes to ECR
- Merge basic Dockerfile/CI setup

Then:
- Docker images for running tests inside of
- Smoke test docker-compose with localstack
