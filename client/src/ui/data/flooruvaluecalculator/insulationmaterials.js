export const insulationMaterials = [
    {
        value: 'Very high performance',
        conductivity: 0.015,
        display: 'Very high performance - aerogel, VIPs, etc. (0.015)',
    },
    {
        value: 'Rigid foam boards',
        conductivity: 0.025,
        display: 'Rigid foam boards - phenolic/PU/PIR (0.025)'
    },
    {
        value: 'Polystyrene foam boards - XPS',
        conductivity: 0.035,
        display: 'Polystyrene foam boards - XPS (0.035)'
    },
    {
        value: 'Polystyrene foam boards - EPS',
        conductivity: 0.04,
        display: 'Polystyrene foam boards - EPS (0.04)'
    },
    {
        value: 'Fibre insulation',
        conductivity: 0.04,
        display: 'Fibre insulation - glasswool, mineral fibre, woodfibre, etc. (0.04)'
    },
    {
        value: 'Rigid glass foam',
        conductivity: 0.05,
        display: 'Rigid glass foam (0.05)'
    },
    {
        value: 'Granular insulation',
        conductivity: 0.1,
        display: 'Granular insulation - technopor, misapor or similar (0.1)'
    },
]
