/*
 * These values are duplicated elsewhere in the non-transpiled portion of the
 * code, in datasets.js.
 */
export default {
    space_heating_demand: [
        { label: "Min target", value: 20 },
        { label: "Max target", value: 70 },
        { label: "UK average", value: 120 },
    ],
    primary_energy_demand: [
        { label: "Target", value: 120 },
        { label: "UK average", value: 360 },
    ],
    co2_per_m2: [
        { label: "Zero Carbon", value: 0 },
        { label: "80% by 2050", value: 17 },
        { label: "UK average", value: 50.3 },
    ],
    energy_use_per_person: [
        { label: "70% heating saving", value: 8.6 },
        { label: "UK average", value: 19.6 },
    ],
};
