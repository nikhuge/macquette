export type Image = {
    id: number;
    url: string;
    width: number;
    height: number;
    thumbnail_url: string;
    thumbnail_width: number;
    thumbnail_height: number;
    note: string;
    is_featured: boolean;
};
