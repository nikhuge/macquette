export const sum = (arr: number[]): number =>
    arr.reduce((total, item) => total + item, 0);
