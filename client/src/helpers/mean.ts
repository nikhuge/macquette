import { sum } from './sum';

export const mean = (arr: number[]) => sum(arr) / arr.length;
