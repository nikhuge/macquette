Design documentation
====================

.. toctree::
   :maxdepth: 1

   001-design-decisions
   002-refactoring-plan
   004-report-generation
